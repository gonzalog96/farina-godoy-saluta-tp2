-- tener en cuenta que el nrorechazo de la tabla rechazo puede estar relacionado con:
-- (*) un rechazo insertado en su tabla homonima.
-- (*) una excepcion que ocurrio al intentar concretar una compra (en este caso, se utiliza el nrooperacion de la compra como nrorechazo).

-- trigger 1 de alertas: todo rechazo debe almacenarse automaticamente en la tabla de alertas
create function sp_alertaTrigger1() returns trigger as $$
begin
    insert into alerta values (default, new.nrotarjeta, now(), new.nrorechazo, 0, 'alerta: se rechazo la compra.');
return new;
end;
$$ language plpgsql;

-- trigger 1: creacion
create trigger tr_alerta1
before insert on rechazo
for each row
execute procedure sp_alertaTrigger1();

-- trigger 2: si una tarjeta registra 2 compras en un lapso menor de 1 min en comercios distintos con el mismo CP.
create function sp_alertaTrigger2() returns trigger as $$
declare
    cant int;
    seg_old int;
    seg_new int;
    comercioviejo_cp char(8);
    comercionuevo_cp char(8);
    
    ult_compra record;
begin
    select count(*) into cant from compra where nrotarjeta = new.nrotarjeta;
    raise notice 'conteo de compras: %', cant;
    if cant >= 1 then 
        -- obtenemos la ultima compra realizada con la tarjeta.
        select * into ult_compra from compra where nrotarjeta = new.nrotarjeta order by fecha desc limit 1;
        
        raise notice 'ultima fecha de compra: %', ult_compra.fecha;
        raise notice 'nueva fecha de compra: %', ult_compra.fecha;
        
        select date_part('second', ult_compra.fecha) into seg_old;
        select date_part('second', new.fecha) into seg_new;
    
        -- se realizo una nueva compra en menos de 1 minuto, en comercios distintos.
        if (seg_new-seg_old) < 60 and ult_compra.nrocomercio <> new.nrocomercio and ult_compra.nrotarjeta = new.nrotarjeta then
    	    -- verificamos que se traten de comercios con mismo CP.
    	    select codigopostal into comercioviejo_cp from comercio where nrocomercio = ult_compra.nrocomercio;
    	    select codigopostal into comercionuevo_cp from comercio where nrocomercio = new.nrocomercio;
    
        	if comercioviejo_cp = comercionuevo_cp then
        		insert into alerta values (default, new.nrotarjeta, now(), new.nrooperacion, 1, 'alerta: una tarjeta registro 2 compras en un lapso menor de 1 minuto en comercios distintos con mismo CP.');
        	end if;
        end if;
    end if;
return new;
end;
$$ language plpgsql;

-- trigger 2: creacion
create trigger tr_alerta2
before insert on compra
for each row
execute procedure sp_alertaTrigger2();

-- trigger 3: si una tarjeta registra 2 compras en un lapso menor de 5min en comercios con diferentes CP.
create function sp_alertaTrigger3() returns trigger as $$
declare
    cant int;
	seg_old int;
	seg_new int;
	comercioviejo_cp char(8);
    comercionuevo_cp char(8);
    
    ult_compra record;
begin
    select count(*) into cant from compra where nrotarjeta = new.nrotarjeta;
    if cant >= 1 then
        -- obtenemos la ultima compra realizada con la tarjeta.
        select * into ult_compra from compra where nrotarjeta = new.nrotarjeta order by fecha desc limit 1;
        
        raise notice 'ultima fecha de compra: %', ult_compra.fecha;
        raise notice 'nueva fecha de compra: %', ult_compra.fecha;
    
        select date_part('second', ult_compra.fecha) into seg_old;
        select date_part('second', new.fecha) into seg_new;
    	select codigopostal into comercioviejo_cp from comercio where nrocomercio = ult_compra.nrocomercio;
    	select codigopostal into comercionuevo_cp from comercio where nrocomercio = new.nrocomercio;
        
        -- se realizo una nueva compra en menos de 5 minutos, en comercios con distintos CP.
        -- 300 seg: 5 min.
        if (seg_new-seg_old) < 300 and comercioviejo_cp <> comercionuevo_cp and ult_compra.nrotarjeta = new.nrotarjeta then
        	insert into alerta values (default, new.nrotarjeta, now(), new.nrooperacion, 5, 'alerta: una tarjeta registro 2 compras en un lapso menor de 5 minuto en comercios con diferentes CP.');
        end if;
    end if;
return new;
end;
$$ language plpgsql;

-- trigger 3: creacion
create trigger tr_alerta3
before insert on compra
for each row
execute procedure sp_alertaTrigger3();

-- trigger 4: si una tarjeta registra un exceso en el limite de compra, se debe generar una alerta.
create function sp_alertaTrigger4() returns trigger as $$
declare
    cant int;
	terminacion_tarjeta int;
	limite_compra decimal;
	monto_actual decimal;
	monto_periodo decimal;
	periodo_desde date;
	periodo_hasta date;
	periodo_actual int;
begin
    select count(*) into cant from compra where nrotarjeta = new.nrotarjeta;
    if cant >= 1 then
        select limitecompra into limite_compra from tarjeta where nrotarjeta = new.nrotarjeta;
    	monto_actual := new.monto;
    
    	-- obtenemos el ultimo digito de la tarjeta (terminacion).
    	select right(new.nrotarjeta, 1) into terminacion_tarjeta;
    
    	-- obtenemos el periodo de inicio y cierre.
    	select extract(month from new.fecha) into periodo_actual;
    	select fechainicio into periodo_desde from cierre where terminacion = terminacion_tarjeta::int and mes = periodo_actual; 
    	select fechacierre into periodo_hasta from cierre where terminacion = terminacion_tarjeta::int and mes = periodo_actual;  
    
    	-- obtenemos el monto a pagar en el periodo actual (hasta el momento).
    	-- coalesce para evitar valores null en monto_periodo.
    	select coalesce(sum(compra.monto),0) into monto_periodo from compra where fecha::date between periodo_desde and periodo_hasta;
    
        -- evaluamos si se supera el limite de compra.
    	if monto_periodo+monto_actual > limite_compra then
    		insert into alerta values (default, new.nrotarjeta, new.fecha, new.nrooperacion, 32, 'alerta: limite de compra excedido para el periodo.');
    	end if;
    end if;
return new;
end;
$$ language plpgsql;

-- trigger 4: creacion.
create trigger tr_alerta4
before insert on compra
for each row
execute procedure sp_alertaTrigger4();

-- trigger 5: si una tarjeta registra 2 rechazos por exceso de limite en el mismo dia, la tarjeta sera suspendida y se grabara una alerta.
create function sp_alertaTrigger5() returns trigger as $$
declare
	cant_rechazos_en_el_dia int;
begin
	select count(*) into cant_rechazos_en_el_dia from rechazo where fecha::date = new.fecha::date and nrorechazo in (select nrorechazo from alerta where codalerta = 32 and nrotarjeta = new.nrotarjeta);

	if cant_rechazos_en_el_dia >= 2 then
		update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
		insert into alerta values (default, new.nrotarjeta, new.fecha, new.nrooperacion, 32, 'alerta: tarjeta suspendida de forma preventiva debido a un exceso en el limite de compra por dia.');
	end if;
	
return new;
end;
$$ language plpgsql;

-- trigger 5: creacion
create trigger tr_alerta5 
before insert on compra
for each row
execute procedure sp_alertaTrigger5();