package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"io/ioutil"
	"log"
	"math/rand"
	"strconv"
	"time"
)

type alerta struct {
	nroalerta   int
	nrotarjeta  string
	fecha       string
	nrorechazo  int
	codalerta   int
	descripcion string
}

type rechazo struct {
	nrorechazo  int
	nrotarjeta  string
	nrocomercio int
	fecha       string
	monto       string
	motivo      string
}

func createDatabase() {
	// conexion a la db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// creamos la base de datos.
	_, err = db.Exec(`drop database if exists tp2_db`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create database tp2_db`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(":: Base de datos creada! ::\n")
}

func crearTablas() {
	// conexion a la db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// tabla 1.
	_, err = db.Exec(`create table cliente (nrocliente int, nombre text, apellido text, domicilio text, telefono char(12))`)
	if err != nil {
		log.Fatal(err)
	}

	// tabla 2.
	_, err = db.Exec(`create table tarjeta (nrotarjeta char(16), nrocliente int, validadesde varchar(6), validahasta char(6), codseguridad char(4), limitecompra decimal(8,2), estado char(10))`)
	if err != nil {
		log.Fatal(err)
	}

	// tabla 3.
	_, err = db.Exec(`create table comercio (nrocomercio int, nombre text, domicilio text, codigopostal char(8), telefono char(12))`)
	if err != nil {
		log.Fatal(err)
	}

	// tabla 4.
	_, err = db.Exec(`create table compra (nrooperacion serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), pagado boolean)`)
	if err != nil {
		log.Fatal(err)
	}

	// tabla 5.
	_, err = db.Exec(`create table rechazo (nrorechazo serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), motivo text)`)
	if err != nil {
		log.Fatal(err)
	}

	// tabla 6.
	_, err = db.Exec(`create table cierre (anio int, mes int, terminacion int, fechainicio date, fechacierre date, fechavto date)`)
	if err != nil {
		log.Fatal(err)
	}

	// tabla 7.
	_, err = db.Exec(`create table cabecera (nroresumen int, nombre text, apellido text, domicilio text, nrotarjeta char(16), desde date, hasta date, vence date, total decimal(8,2))`)
	if err != nil {
		log.Fatal(err)
	}

	// tabla 8.
	_, err = db.Exec(`create table detalle (nroresumen int, nrolinea int, fecha date, nombrecomercio text, monto decimal(7,2))`)
	if err != nil {
		log.Fatal(err)
	}

	// tabla 9.
	_, err = db.Exec(`create table alerta (nroalerta serial, nrotarjeta char(16), fecha timestamp, nrorechazo int, codalerta int, descripcion text)`)
	if err != nil {
		log.Fatal(err)
	}

	// tabla 10 de prueba.
	_, err = db.Exec(`create table consumo (nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto decimal(7,2))`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(":: Tablas creadas! ::\n")
}

func agregarPK() {

	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`alter table cliente add constraint cliente_pk primary key(nrocliente)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table tarjeta add constraint tarjeta_pk primary key(nrotarjeta)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table comercio add constraint comercio_pk primary key(nrocomercio)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table compra add constraint compra_pk primary key(nrooperacion)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table rechazo add constraint rechazo_pk primary key(nrorechazo)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table cierre add constraint cierre_pk primary key(anio, mes, terminacion)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table cabecera add constraint cabecera_pk primary key(nroresumen)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table detalle add constraint detalle_pk primary key(nroresumen, nrolinea)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table alerta add constraint alerta_pk primary key(nroalerta)`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(":: PKs añadidas! ::\n")
}

func agregarFK() {

	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`alter table tarjeta add constraint tarjeta_fk foreign key (nrocliente) references cliente(nrocliente)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table compra add constraint compra_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table cabecera add constraint cabecera_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta)`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table rechazo add constraint rechazo_fk foreign key(nrocomercio) references comercio(nrocomercio)`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(":: FKs añadidas! ::\n")
}

// borrar PKS.

func borrarPKS() {
	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`alter table cliente drop constraint cliente_pk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table tarjeta drop constraint tarjeta_pk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table comercio drop constraint comercio_pk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table compra drop constraint compra_pk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table rechazo drop constraint rechazo_pk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table cierre drop constraint cierre_pk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table cabecera drop constraint cabecera_pk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table detalle drop constraint detalle_pk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table alerta drop constraint alerta_pk`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(":: Borrado de PKs - exito! ::\n")
}

// borrar FKS.
func borrarFKS() {

	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`alter table tarjeta drop constraint tarjeta_fk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table compra drop constraint compra_fk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table cabecera drop constraint cabecera_fk`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`alter table rechazo drop constraint rechazo_fk`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(":: Borrado de FKs - exito! ::\n")
}

func cargarClientes() {

	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`insert into cliente values (1, 'Juan', 'Gomez', '9 de julio 1233', '1123554699')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (2, 'Julian', 'Gutierrez', '10 de septiembre 1220', '1124458856')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (3, 'Gonzalo', 'Acosta', 'Godoy cruz 1100', '1147405563')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (4, 'Federico', 'Guirado', 'Ocampo 1455', '1147365526')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (5, 'Ariel', 'Zaccardi', 'Ing. Delpini 4455', '1133548896')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (6, 'Natalia', 'Gutierrez', 'Frederick 459', '1144859966')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (7, 'Ines', 'Pitar', 'Delfino 1456', '1147361590')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (8, 'Matias', 'Russo', '41 Lima 123', '1147558566')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (9, 'Nicolas', 'Zubieta', 'Gutierrez 7355', '1194922320')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (10, 'David', 'Lopez', 'Lote 2 Santa Barbara', '1152206555')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (11, 'Daniela', 'Todesco', 'Lote 10 Santa Barbara', '1153320025')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (12, 'Ivonne', 'Gando', 'Carlos Villate 458', '1132200588')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (13, 'Fabian', 'Vera', 'Ugarte 110', '1147556599')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (14, 'Lucas', 'Bianchini', 'Ugarte 1355', '1147589965')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (15, 'José', 'Alarcón', 'Av de Mayo 458', '1143551775')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (16, 'Alejandro', 'Goya', 'Av de Mayo 654', '1144155499')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (17, 'Rocío', 'Quintero', 'Gutemberg 50', '1147406889')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (18, 'Rodrigo', 'Aguilar', 'Talan Talan 395', '1147445866')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (19, 'Daiana', 'Chahwan', 'Francesita 25', '1147401455')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into cliente values (20, 'Emiliano', 'Contreras', 'Palermo 4588', '1129900145')`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(":: Carga de clientes finalizada! ::\n")
}

func cargarComercios() {

	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`insert into comercio values (1, 'JPS Computacion', 'Jose Maria Paz 461', '1617', '1140580740')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into comercio values (2, 'CJK Computación', 'San Luis 636', '1617', '1147261236')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into comercio values (3, 'PC10 Computación', 'Boulogne Sur Mer 82', '1617', '1147266888')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into comercio values (4, 'Station Computación', 'Hipólito Yrigoyen 1777', '1618', '1147261166')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into comercio values (5, 'NHL Computación', 'Las Violetas 1873', '1618', '1144442555')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into comercio values (6, 'OyD Computación', 'Hipólito Yrigoyen 306', 1617, '1147402741')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (7, 'Gimaq', 'Hipólito Yrigoyen 971', '1617', '1147363143')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (8, 'EyG Computación', 'Junín 995', '1617', '1165371741')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (9, 'Compumas', 'Buenos Aires 930', '1617', '1147262568')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (10, 'Electromar', 'Hipólito Yrigoyen 1096', '1617', '1147407182')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (11, 'SE Computación', 'Palacios 785', '1608', '1163672038')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (12, 'Packtronic', 'Hipólito Yrigoyen 904', '1617', '1147407189')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (13, 'Zero Computación', 'Marcelo T. de Alvear 1782', '1611', '1147482029')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (14, 'Omega Computación', 'Hipólito Yrigoyen 2136', '1618', '1147263615')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (15, 'COM Computación', 'Marcelo 1513', '1611', '1148460523')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (16, 'TecnoLand', 'Hipólito Yrigoyen 573', '1617', '1147260224')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (17, 'Pcx Computación', 'Alvear 450', '1611', '1148460750')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (18, 'Musimundo', 'Hipólito Yrigoyen 863', '1617', '1145080100')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (19, 'Rincón Computación', 'Santa María 2815', '1624', '1147556522')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`INSERT into comercio values (20, 'Regalitos Regalados', 'Hipólito Yrigoyen 989', '1617', '1126912193')`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(":: Carga de comercios finalizada! ::\n")
}

func cargarTarjetas() {
	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`insert into tarjeta values ('4517560630935320', 1, '201201', '201802', '1344', 10000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517560630935321', 2, '201202', '201803', '1965', 15000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935322', 3, '201701', '202302', '1775', 18000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935323', 4, '201702', '202303', '1445', 18500, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935324', 5, '201703', '202304', '1754', 20000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935325', 6, '201704', '202305', '1225', 12300, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935326', 7, '201705', '202306', '4788', 16700, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935327', 8, '201706', '202307', '4577', 19800, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935328', 9, '201707', '202308', '4411', 22000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935329', 10, '201708', '202309', '4412', 22500, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517560630935330', 11, '201709', '202310', '4214', 25000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517560630935331', 12, '201710', '202311', '4188', 7000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517560630935332', 13, '201711', '202312', '4744', 8000.18, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935333', 14, '201807', '202408', '4255', 13400.15, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935334', 15, '201801', '202402', '3366', 12500.12, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935335', 16, '201802', '202403', '1255', 10000, 'anulada')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935336', 17, '201803', '202404', '1155', 9000, 'suspendida')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935337', 18, '201804', '202405', '1125', 30000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935338', 18, '201805', '202406', '1129', 15000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935339', 19, '201806', '202407', '1177', 14000, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935340', 19, '201808', '202409', '1655', 11680, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`insert into tarjeta values ('4517650630935341', 20, '201810', '202411', '2530', 13010, 'vigente')`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(":: Carga de tarjetas finalizada! ::\n")
}

func cargarConsumosVirtuales() {
	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	/*
	   // 1er test: tarjeta invalida.
	   _, err = db.Exec(`insert into consumo values ('1111444488889999', '1234', 1, 2000.10)`)
	   if err != nil {
	   log.Fatal(err)
	   }

	   // 2do test: tarjeta valida con codigo de seguridad incorrecto.
	   _, err = db.Exec(`insert into consumo VALUES ('4517560630935320', '1111', 1, 2000.10)`)
	   if err != nil {
	   log.Fatal(err)
	   }

	   // 3er test: tarjeta y codigos validos, pero supera el limite de compra.
	   _, err = db.Exec(`insert into consumo values ('4517560630935320', '1344', 1, 25000)`)
	   if err != nil {
	   log.Fatal(err)
	   }

	   // 4to test: tarjeta y codigos validos, pero vencida.
	   _, err = db.Exec(`insert into consumo values ('4517650630935335', '1255', 1, 2000)`)
	   if err != nil {
	   log.Fatal(err)
	   }

	   // 5to test: tarjeta y codigos validos, pero suspendida.
	   _, err = db.Exec(`insert into consumo values ('4517650630935336', '1155', 1, 3000)`)
	   if err != nil {
	   log.Fatal(err)
	   }
	*/
	// 6to test: generacion del resumen -> 3 consumos.
	_, err = db.Exec(`insert into consumo values ('4517650630935334', '3366', 1, 200)`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`insert into consumo values ('4517650630935334', '3366', 3, 400)`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`insert into consumo values ('4517650630935334', '3366', 2, 400)`)
	if err != nil {
		log.Fatal(err)
	}
	/*
	   // 7mo test:  ingreso de 2 compras con lapso menor a 1 minuto, en comercios distintos con mismo CP.
	   _, err = db.Exec(`insert into consumo values ('4517650630935333', '4255', 16, 150)`)
	   if err != nil {
	   log.Fatal(err)
	   }

	   _, err = db.Exec(`insert into consumo values ('4517650630935333', '4255', 18, 150)`)
	   if err != nil {
	   log.Fatal(err)
	   }*/

	fmt.Printf(":: Carga de consumos virtuales finalizada! ::\n")
}

func autorizarConsumos() {
	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`select consumos_testing()`)
	if err != nil {
		log.Fatal(err)
	}
}

func random(min, max int) int {
	return rand.Intn(max-min) + min
}

func cargarCierres() {
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// mes 12: diciembre -> genera nuevas fechas de cierre y vto para 2019 (no se incluye)
	cant_meses := 11
	cant_terminaciones := 10
	fecha := ""

	for i := 1; i <= cant_meses; i++ {
		for j := 0; j < cant_terminaciones; j++ {

			dia_inicio := 10 + random(-9, 9)

			mes := i
			terminacion := j
			// calculo de la fecha de inicio
			if mes > 9 {
				if dia_inicio > 9 {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(mes) + "-" + strconv.Itoa(dia_inicio)
				} else {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(mes) + "-" + strconv.Itoa(0) + strconv.Itoa(dia_inicio)
				}
			} else {
				if dia_inicio > 9 {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(0) + strconv.Itoa(mes) + "-" + strconv.Itoa(dia_inicio)
				} else {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(0) + strconv.Itoa(mes) + "-" + strconv.Itoa(0) + strconv.Itoa(dia_inicio)
				}
			}
			fechaInicio, err := time.Parse("2006-01-02", fecha)
			if err != nil {
				fmt.Println(err)
			}

			// calculo de la fecha de cierre
			if mes+1 > 9 {
				if dia_inicio > 9 {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(mes+1) + "-" + strconv.Itoa(dia_inicio)
				} else {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(mes+1) + "-" + strconv.Itoa(0) + strconv.Itoa(dia_inicio)
				}
			} else {
				if dia_inicio > 9 {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(0) + strconv.Itoa(mes+1) + "-" + strconv.Itoa(dia_inicio)
				} else {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(0) + strconv.Itoa(mes+1) + "-" + strconv.Itoa(0) + strconv.Itoa(dia_inicio)
				}
			}

			fechaCierre, err := time.Parse("2006-01-02", fecha)
			if err != nil {
				fmt.Println(err)
			}

			// calculo de la fecha de vencimiento
			if mes+1 > 9 {
				if dia_inicio+7 > 9 {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(mes+1) + "-" + strconv.Itoa(dia_inicio+7)
				} else {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(mes+1) + "-" + strconv.Itoa(0) + strconv.Itoa(dia_inicio+7)
				}
			} else {
				if dia_inicio+7 > 9 {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(0) + strconv.Itoa(mes+1) + "-" + strconv.Itoa(dia_inicio+7)
				} else {
					fecha = strconv.Itoa(2018) + "-" + strconv.Itoa(0) + strconv.Itoa(mes+1) + "-" + strconv.Itoa(0) + strconv.Itoa(dia_inicio+7)
				}
			}

			fechaVto, err := time.Parse("2006-01-02", fecha)
			if err != nil {
				fmt.Println(err)
			}

			// ejecucion final - mes, terminacion, fechaInicio, fechaCierre, fechaVto
			_, err = db.Exec(`insert into cierre values (2018, $1, $2, $3, $4, $5)`, mes, terminacion, fechaInicio, fechaCierre, fechaVto)
			if err != nil {
				log.Fatal(err)
			}
		}
	}
	fmt.Printf(":: Creación de cierres finalizada! ::\n")
}

func cargarSP() {
	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	datos, err := ioutil.ReadFile(`procedimientos.sql`)
	if err != nil {
		log.Fatal(err)
	}

	_, err2 := db.Exec(string(datos))
	if err2 != nil {
		log.Fatal(err2)
	}

	fmt.Printf(":: Carga de Stored Procedures finalizada! ::\n")
}

func cargarTR() {
	// abrir db.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	datos, err := ioutil.ReadFile(`triggers.sql`)
	if err != nil {
		log.Fatal(err)
	}

	_, err2 := db.Exec(string(datos))
	if err2 != nil {
		log.Fatal(err2)
	}

	fmt.Printf(":: Carga de Triggers finalizada! ::\n")
}

func verAlertas() {
	// open DB.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rows, err := db.Query(`select * from alerta`)
	if err != nil {
		log.Fatal(err)
	}

	var a alerta
	fmt.Printf("::: Alertas :::\n")
	fmt.Printf("---\n")
	fmt.Printf("\n")
	for rows.Next() {
		if err := rows.Scan(&a.nroalerta, &a.nrotarjeta, &a.fecha, &a.nrorechazo, &a.codalerta, &a.descripcion); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%v %v %v %v %v %v\n\n", a.nroalerta, a.nrotarjeta, a.fecha, a.nrorechazo, a.codalerta, a.descripcion)
	}
	fmt.Printf("---\n")
	fmt.Printf("::: FIN alertas :::\n")
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
}

func verRechazos() {
	// open DB.
	db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=tp2_db sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rows, err := db.Query(`select * from rechazo`)
	if err != nil {
		log.Fatal(err)
	}

	var a rechazo
	fmt.Printf("::: Rechazos :::\n")
	fmt.Printf("---\n")
	fmt.Printf("\n")
	for rows.Next() {
		if err := rows.Scan(&a.nrorechazo, &a.nrotarjeta, &a.nrocomercio, &a.fecha, &a.monto, &a.motivo); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%v %v %v %v %v %v\n\n", a.nrorechazo, a.nrotarjeta, a.nrocomercio, a.fecha, a.monto, a.motivo)
	}
	fmt.Printf("---\n")
	fmt.Printf("::: FIN rechazos :::\n")
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	var opcion string

	fmt.Printf(":: Gestión y Administración de Bases de Datos ::\n")
	fmt.Printf(":: Segundo cuatrimestre del 2018 ::\n")
	fmt.Printf(":: Docentes: Rondelli, Daniel | Trigila, Mariano ::\n")
	fmt.Printf(":: Alumnos: Fariña, Gabriel | Godoy, Gonzalo | Saluta, Nahuel ::\n")
	fmt.Printf("---\n")
	fmt.Printf("Ingrese el numero que corresponde a las siguientes opciones y luego presione ENTER para gestionar: \n")
	fmt.Printf("---\n")
	fmt.Printf("1. Crear BD.\n")
	fmt.Printf("2. Crear tablas.\n")
	fmt.Printf("3. Cargar PKS y FKS.\n")
	fmt.Printf("4. Cargar clientes, comercios y tarjetas.\n")
	fmt.Printf("5. Cargar cierres.\n")
	fmt.Printf("6. Cargar procedimientos y triggers.\n")
	fmt.Printf("7. Cargar consumos de prueba y autorizar.\n")
	fmt.Printf("8. Ver rechazos.\n")
	fmt.Printf("9. Ver alertas.\n")
	fmt.Printf("10. Borrar FKS y PKS.\n")
	fmt.Printf("---\n")
	fmt.Scanf("%s", &opcion)

	// verificamos la opcion elegida por el usuario.
	if opcion == "1" {
		createDatabase()
	} else if opcion == "2" {
		crearTablas()
	} else if opcion == "3" {
		agregarPK()
		agregarFK()
	} else if opcion == "4" {
		cargarClientes()
		cargarComercios()
		cargarTarjetas()
	} else if opcion == "5" {
		cargarCierres()
	} else if opcion == "6" {
		cargarSP()
		cargarTR()
	} else if opcion == "7" {
		cargarConsumosVirtuales()
		autorizarConsumos()
	} else if opcion == "8" {
		verRechazos()
	} else if opcion == "9" {
		verAlertas()
	} else if opcion == "10" {
		borrarFKS()
		borrarPKS()
	}
}
