-- procedimiento para autorizar compras.
create or replace function autorizar_compra(num_tarjeta char, cod_seg char, num_comercio int, monto decimal) returns boolean as $$
declare
	resultado record;
	tarj_limitecompra decimal;
	suma decimal;
	v record;
begin

-- validacion 1: la tarjeta no puede estar vencida.
	select * into resultado from tarjeta where nrotarjeta = num_tarjeta;
	if resultado.estado = 'anulada' then
	    insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'plazo de vigencia expirado');
	    raise notice 'plazo de vigencia expirado';
	    return false;
	end if;

-- validacion 2: la tarjeta no puede estar suspendida.
	select * into resultado from tarjeta where nrotarjeta = num_tarjeta;
	if resultado.estado = 'suspendida' then
		insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'la tarjeta se encuentra suspendida');
	    raise notice 'la tarjeta se encuentra suspendida';
        return false;
	end if;
	
-- validacion 3: el numero de tarjeta debe existir y, ademas,
-- debe ser una tarjeta vigente.
	select * into resultado from tarjeta where nrotarjeta = num_tarjeta and estado = 'vigente';
	if not found then
	    insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'tarjeta no valida o no vigente');
	    raise notice 'tarjeta no valida o no vigente';
	    return false;
	end if;

-- validacion 4: el codigo de seg. debe ser correcto.
	select * into resultado from tarjeta where nrotarjeta = num_tarjeta and codseguridad = cod_seg;
	if not found then
	    insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'codigo de seguridad invalido');
	    raise notice 'codigo de seguridad invalido';
	    return false;
	end if;

-- validacion 5: el monto total de compras pendientes + la compra a realizar NO debe superar el limite.
    select coalesce(sum(compra.monto),0) into suma from compra where nrotarjeta = num_tarjeta and pagado = false;

	select limitecompra into tarj_limitecompra from tarjeta where nrotarjeta = num_tarjeta;
	if suma+monto > tarj_limitecompra then
		insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'supera limite de tarjeta');
	    raise notice 'supera limite de tarjeta';
	    return false;
	end if;

-- se superaron las etapas de validacion: ingresar compra.
	insert into compra values (default, num_tarjeta, num_comercio, now(), monto, false);
	return true;
end;
$$ language plpgsql;

-- procedimiento para la generacion de los resumenes: recibe un numero de cliente y un periodo.
create or replace function generar_resumen(num_cliente int, periodo int) returns void as $$
declare
	nomb text;
	apell text;
	dir text;
	
	nombre_comercio text;
	terminacion_tarjeta int;
	periodo_desde date;
	periodo_hasta date;
	fecha_vto date;
	
	monto decimal;
	mont_final decimal;

	id_resumen int;
	id_linea int;
	nrotarj int;
	fecha_compra date;
	
	v record;
	j record;

begin
	-- buscamos el nombre, apellido y direccion del cliente, segun su numero de identificacion.
	select nombre into nomb from cliente where nrocliente = num_cliente;
	select apellido into apell from cliente where nrocliente = num_cliente;
	select domicilio into dir from cliente where nrocliente = num_cliente;

	-- FOR (cabecera) para recorrer cada una de las tarjetas de los clientes (en el caso de que tenga mas de una)
	id_resumen := 1;
	for v in select * from tarjeta where nrocliente = num_cliente loop
		-- obtenemos el ultimo digito de la tarjeta (terminacion).
		select right(v.nrotarjeta, 1) into terminacion_tarjeta;

		-- obtenemos el periodo de inicio, cierre y fecha de vencimiento.
		select fechainicio into periodo_desde from cierre where terminacion = terminacion_tarjeta::int and mes = periodo;
		select fechacierre into periodo_hasta from cierre where terminacion = terminacion_tarjeta::int and mes = periodo;
		select fechavto into fecha_vto from cierre where terminacion = terminacion_tarjeta::int and mes = periodo;

		-- FOR  (detalle) para obtener las compras y montos segun lo requerido
		-- generamos los consumos por periodo.
		id_linea := 1;
		mont_final = 0;
	    
	   raise notice 'periodo desde: %', periodo_desde;
	   raise notice 'periodo_hasta: %', periodo_hasta;
	        
		for j in select * from compra where nrotarjeta = v.nrotarjeta and (fecha::date >= periodo_desde and fecha::date <= periodo_hasta) loop
	    
		    select nombre into nombre_comercio from comercio where nrocomercio = j.nrocomercio;
		    select j.fecha into fecha_compra;
		    select j.monto into monto;
			mont_final := mont_final + j.monto;

			-- insertamos la compra en el detalle.
			insert into detalle values(id_resumen, id_linea, fecha_compra, nombre_comercio, monto);
			id_linea := id_linea + 1;
		end loop;
		id_linea := 0;
		
		-- una vez hayamos agregado todos los detalles, insertamos la cabecera del resumen.
		insert into cabecera values (id_resumen, nomb, apell, dir, v.nrotarjeta, periodo_desde, periodo_hasta, fecha_vto, mont_final);
		id_resumen := id_resumen + 1;
	end loop;
end;
$$ language plpgsql;

-- procedimiento de testing para consumos.
create or replace function consumos_testing() returns void as $$
declare
    cv record;
begin
    -- recorremos todos los consumos virtuales.
    for cv in select * from consumo loop
        -- intentamos autorizar cada uno de ellos.
        perform autorizar_compra(cv.nrotarjeta, cv.codseguridad, cv.nrocomercio, cv.monto);
    end loop;
end;
$$ language plpgsql;
