package main

import (
    "encoding/json"
    "fmt"
    bolt "github.com/coreos/bbolt"
    "log"
    "strconv"
)

type Cliente struct {
    Nrocliente   int
    Nombre   string
    Apellido string
    Domicilio string
    Telefono string
}

type Tarjeta struct {
    Nrotarjeta   string
    Nrocliente   int
    Validadesde string
    Validahasta string
    Codseguridad string
    Limitecompra string
    Estado string
}

type Comercio struct {
    Nrocomercio   int
    Nombre   string
    Domicilio string
    Codigopostal string
    Telefono string
}

type Compra struct {
    Nrooperacion   int
    Nrotarjeta   string
    Nrocomercio int
    Fecha string
    Monto string
    Pagado bool
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
    // abre transacción de escritura
    tx, err := db.Begin(true)
    if err != nil {
        return err
    }
    defer tx.Rollback()

    b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

    err = b.Put(key, val)
    if err != nil {
        return err
    }

    // cierra transacción
    if err := tx.Commit(); err != nil {
        return err
    }

    return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
    var buf []byte

    // abre una transacción de lectura
    err := db.View(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(bucketName))
        buf = b.Get(key)
        return nil
    })

    return buf, err
}

func main() {
    db, err := bolt.Open("tp2.db", 0600, nil)
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    cliente_1 := Cliente{1, "Juan", "Gomez", "9 de julio 1233", "1123554699"}
    data1, err := json.Marshal(cliente_1)
    if err != nil {
        log.Fatal(err)
    }

    cliente_2 := Cliente{2, "Julian", "Gutierrez", "10 de septiembre 1220", "1124458856"}
    data2, err := json.Marshal(cliente_2)
    if err != nil {
        log.Fatal(err)
    }

    cliente_3 := Cliente{3, "Gonzalo", "Acosta", "Godoy cruz 1100", "1147405563"}
    data3, err := json.Marshal(cliente_3)
    if err != nil {
        log.Fatal(err)
    }

    tarjeta_1 := Tarjeta{"4517560630935320", 1, "201201", "201802", "1344", "10000", "vigente"}
    data4, err := json.Marshal(tarjeta_1)
    if err != nil {
        log.Fatal(err)
    }

    tarjeta_2 := Tarjeta{"4517560630935321", 2, "201202", "201803", "1965", "15000", "vigente"}
    data5, err := json.Marshal(tarjeta_2)
    if err != nil {
        log.Fatal(err)
    }

    tarjeta_3 := Tarjeta{"4517650630935322", 3, "201701", "202302", "1775", "18000", "vigente"}
    data6, err := json.Marshal(tarjeta_3)
    if err != nil {
        log.Fatal(err)
    }

    comercio_1 := Comercio{1, "JPS Computacion", "Jose Maria Paz 461", "1617", "1140580740"}
    data7, err := json.Marshal(comercio_1)
    if err != nil {
        log.Fatal(err)
    }

    comercio_2 := Comercio{2, "CJK Computación", "San Luis 636", "1617", "1147261236"}
    data8, err := json.Marshal(comercio_2)
    if err != nil {
        log.Fatal(err)
    }

    comercio_3 := Comercio{3, "PC10 Computación", "Boulogne Sur Mer 82", "1617", "1147266888"}
    data9, err := json.Marshal(comercio_3)
    if err != nil {
        log.Fatal(err)
    }

    compra_1 := Compra{1, tarjeta_3.Nrotarjeta, 1, "2018/11/26", "200", true}
    data10, err := json.Marshal(compra_1)
    if err != nil {
        log.Fatal(err)
    }

    compra_2 := Compra{2, "4517650630935334", 3, "2018/11/26", "400", true}
    data11, err := json.Marshal(compra_2)
    if err != nil {
        log.Fatal(err)
    }

    compra_3 := Compra{3, "4517650630935334", 2, "2018/11/26", "400", true}
    data12, err := json.Marshal(compra_3)
    if err != nil {
        log.Fatal(err)
    }

    CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente_1.Nrocliente)), data1)
    CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente_2.Nrocliente)), data2)
    CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente_3.Nrocliente)), data3)

    CreateUpdate(db, "tarjeta", []byte(tarjeta_1.Nrotarjeta), data4)
    CreateUpdate(db, "tarjeta", []byte(tarjeta_2.Nrotarjeta), data5)
    CreateUpdate(db, "tarjeta", []byte(tarjeta_3.Nrotarjeta), data6)

    CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio_1.Nrocomercio)), data7)
    CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio_2.Nrocomercio)), data8)
    CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio_3.Nrocomercio)), data9)

    CreateUpdate(db, "compra", []byte(strconv.Itoa(compra_1.Nrooperacion)), data10)
    CreateUpdate(db, "compra", []byte(strconv.Itoa(compra_2.Nrooperacion)), data11)
    CreateUpdate(db, "compra", []byte(strconv.Itoa(compra_3.Nrooperacion)), data12)

    resultado1, err := ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente_1.Nrocliente)))
   	fmt.Printf("%s\n", resultado1)
    resultado2, err := ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente_2.Nrocliente)))
   	fmt.Printf("%s\n", resultado2)
    resultado3, err := ReadUnique(db, "cliente", []byte(strconv.Itoa(cliente_3.Nrocliente)))
   	fmt.Printf("%s\n", resultado3)

   	resultado4, err := ReadUnique(db, "tarjeta", []byte(tarjeta_1.Nrotarjeta))
   	fmt.Printf("%s\n", resultado4)
   	resultado5, err := ReadUnique(db, "tarjeta", []byte(tarjeta_2.Nrotarjeta))
   	fmt.Printf("%s\n", resultado5)
   	resultado6, err := ReadUnique(db, "tarjeta", []byte(tarjeta_3.Nrotarjeta))
   	fmt.Printf("%s\n", resultado6)

    resultado7, err := ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio_1.Nrocomercio)))
   	fmt.Printf("%s\n", resultado7)
    resultado8, err := ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio_2.Nrocomercio)))
   	fmt.Printf("%s\n", resultado8)
    resultado9, err := ReadUnique(db, "comercio", []byte(strconv.Itoa(comercio_3.Nrocomercio)))
   	fmt.Printf("%s\n", resultado9)

    resultado10, err := ReadUnique(db, "compra", []byte(strconv.Itoa(compra_1.Nrooperacion)))
   	fmt.Printf("%s\n", resultado10)
    resultado11, err := ReadUnique(db, "comercio", []byte(strconv.Itoa(compra_2.Nrooperacion)))
   	fmt.Printf("%s\n", resultado11)
    resultado12, err := ReadUnique(db, "comercio", []byte(strconv.Itoa(compra_3.Nrooperacion)))
   	fmt.Printf("%s\n", resultado12)
}
