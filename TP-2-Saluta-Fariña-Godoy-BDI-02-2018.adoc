= Gestión y Administración de Bases de Datos: TP 2
Gabriel Fariña <gabriel.adrian.f.fernandes@gmail.com>;Nahuel Saluta <saluta_nahuel@hotmail.com>;Gonzalo Godoy <godoyg1996@gmail.com>
v1, {docdate}. Daniel Rondelli y Mariano Trigila
:toc: 
:toc-title: Contenidos 
:numbered: 
:source-highlighter: coderay 
:tabsize: 4 

== Introducción

El objetivo del trabajo es diseñar, utilizando el lenguage _SQL_,
las tablas, procedimientos y triggers necesarios para permitir el almacenamiento
de la información de un sistema comercial que empleará, como único medio de pago,
tarjetas de crédito. Además, se persigue el fin de generar automaticamente alertas ante
actividades inusuales.

El desarrollo se efectuó en _Go_. Adicionalmente, se permite el almacenamiento
de la información en archivos _JSON_.

== Descripción

=== Funcionamiento

El programa permite una interacción directa con el usuario, de forma tal
que el mismo puede ingresar la opción adecuada para gestionar la creación de las
tablas, _primary keys_, _foreign keys_, procedimientos, _triggers_ y cargar
los consumos de prueba.

Adicionalmente, se puede consultar el historial de cabeceras y detalles de los resúmenes
generados para cada tarjeta, según el período.

=== Ejemplo de procedimiento para autorizar las compras.

[source, C]
----
-- procedimiento para autorizar compras.
create or replace function autorizar_compra(num_tarjeta char, cod_seg char, num_comercio int, monto decimal) returns boolean as $$
declare
	resultado record;
	tarj_limitecompra decimal;
	suma decimal;
	v record;
begin

-- validacion 1: la tarjeta no puede estar vencida.
	select * into resultado from tarjeta where nrotarjeta = num_tarjeta;
	if resultado.estado = 'anulada' then
	    insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'plazo de vigencia expirado');
	    raise notice 'plazo de vigencia expirado';
	    return false;
	end if;

-- validacion 2: la tarjeta no puede estar suspendida.
	select * into resultado from tarjeta where nrotarjeta = num_tarjeta;
	if resultado.estado = 'suspendida' then
		insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'la tarjeta se encuentra suspendida');
	    raise notice 'la tarjeta se encuentra suspendida';
        return false;
	end if;
	
-- validacion 3: el numero de tarjeta debe existir y, ademas,
-- debe ser una tarjeta vigente.
	select * into resultado from tarjeta where nrotarjeta = num_tarjeta and estado = 'vigente';
	if not found then
	    insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'tarjeta no valida o no vigente');
	    raise notice 'tarjeta no valida o no vigente';
	    return false;
	end if;

-- validacion 4: el codigo de seg. debe ser correcto.
	select * into resultado from tarjeta where nrotarjeta = num_tarjeta and codseguridad = cod_seg;
	if not found then
	    insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'codigo de seguridad invalido');
	    raise notice 'codigo de seguridad invalido';
	    return false;
	end if;

-- validacion 5: el monto total de compras pendientes + la compra a realizar NO debe superar el limite.
    select coalesce(sum(compra.monto),0) into suma from compra where nrotarjeta = num_tarjeta;

	select limitecompra into tarj_limitecompra from tarjeta where nrotarjeta = num_tarjeta;
	if suma+monto > tarj_limitecompra then
		insert into rechazo values (default, num_tarjeta, num_comercio, now(), monto, 'supera limite de tarjeta');
	    raise notice 'supera limite de tarjeta';
	    return false;
	end if;

-- se superaron las etapas de validacion: ingresar compra.
	insert into compra values (default, num_tarjeta, num_comercio, now(), monto, true);
	return true;
end;
$$ language plpgsql;
----

=== Dificultades

Se presentaron varias dificultades en la elaboración. Principalmente, _aprender_
a utilizar el lenguaje _Go_ y adaptarse a las nuevas reglas.

Algunas de las problemáticas fueron las siguientes:

- _Castear_ un tipo de datos _timestamp_ a una fecha del tipo _"AA/MM/DD"_.

- _Dropear_ las _Foreign Keys_, en primer lugar, antes de las _Primary Keys_.

- Obtener un número _random_ para la generación de fechas de cierre.

- Cargar procedimientos y _triggers_ con _Golang_.

- Autoincrementar valores con _serial_.

- Obtener el último caracter de una char(16).

- Obtener los valores de una tabla ordenados en forma decreciente y con cierto límite.

- Obtener los segundos de un valor _timestamp_.

- Uso de la función _coalesce()_ para devolver un valor por defecto ante _nulls_.

- Obtener la suma de los valores de una tabla mediante _sum()_.

== Implementación

=== Creación de la base de datos de _testing_

[source, go]
----
func createDatabase() {
// conexion a la db.
db, err := sql.Open("postgres", "user=postgres password=armado01 dbname=postgres sslmode=disable")
if err != nil {
log.Fatal(err)
}
defer db.Close()

// creamos la base de datos.
_, err = db.Exec(`drop database if exists tp2_db`)
if err != nil {
log.Fatal(err)
}
_, err = db.Exec(`create database tp2_db`)
if err != nil {
log.Fatal(err)
}
}
----

=== Creación de la tabla CLIENTE

[source, go]
----
_, err = db.Exec(`create table cliente (nrocliente int, nombre text, apellido text, domicilio text, telefono char(12))`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de la tabla TARJETA

[source, go]
----
_, err = db.Exec(`create table tarjeta (nrotarjeta char(16), nrocliente int, validadesde varchar(6), validahasta char(6), codseguridad char(4), limitecompra decimal(8,2), estado char(10))`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de la tabla COMERCIO

[source, go]
----
_, err = db.Exec(`create table comercio (nrocomercio int, nombre text, domicilio text, codigopostal char(8), telefono char(12))`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de la tabla COMPRA

[source, go]
----
_, err = db.Exec(`create table compra (nrooperacion serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), pagado boolean)`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de la tabla RECHAZO

[source, go]
----
_, err = db.Exec(`create table rechazo (nrorechazo serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), motivo text)`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de la tabla CIERRE

[source, C]
----
_, err = db.Exec(`create table cierre (anio int, mes int, terminacion int, fechainicio date, fechacierre date, fechavto date)`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de la tabla CABECERA

[source, C]
----
_, err = db.Exec(`create table cabecera (nroresumen int, nombre text, apellido text, domicilio text, nrotarjeta char(16), desde date, hasta date, vence date, total decimal(8,2))`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de la tabla DETALLE

[source, C]
----
_, err = db.Exec(`create table detalle (nroresumen int, nrolinea int, fecha date, nombrecomercio text, monto decimal(7,2))`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de la tabla ALERTA

[source, C]
----
_, err = db.Exec(`create table alerta (nroalerta serial, nrotarjeta char(16), fecha timestamp, nrorechazo int, codalerta int, descripcion text)`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de la tabla CONSUMO

[source, C]
----
_, err = db.Exec(`create table consumo (nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto decimal(7,2))`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de las *PRIMARY KEYS*

[source, C]
----
_, err = db.Exec(`alter table cliente add constraint cliente_pk primary key(nrocliente)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table tarjeta add constraint tarjeta_pk primary key(nrotarjeta)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table comercio add constraint comercio_pk primary key(nrocomercio)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table compra add constraint compra_pk primary key(nrooperacion)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table rechazo add constraint rechazo_pk primary key(nrorechazo)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table cierre add constraint cierre_pk primary key(anio, mes, terminacion)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table cabecera add constraint cabecera_pk primary key(nroresumen)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table detalle add constraint detalle_pk primary key(nroresumen, nrolinea)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table alerta add constraint alerta_pk primary key(nroalerta)`)
if err != nil {
log.Fatal(err)
}
----

=== Creación de las *FOREIGN KEYS*

[source, C]
----
_, err = db.Exec(`alter table tarjeta add constraint tarjeta_fk foreign key (nrocliente) references cliente(nrocliente)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table compra add constraint compra_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table cabecera add constraint cabecera_fk foreign key (nrotarjeta) references tarjeta(nrotarjeta)`)
if err != nil {
log.Fatal(err)
}

_, err = db.Exec(`alter table rechazo add constraint rechazo_fk foreign key(nrocomercio) references comercio(nrocomercio)`)
if err != nil {
log.Fatal(err)
}
----

=== Función MAIN

[source, C]
----
    var opcion string

    fmt.Printf(":: Gestión y Administración de Bases de Datos ::\n")
    fmt.Printf(":: Segundo cuatrimestre del 2018 ::\n")
    fmt.Printf(":: Docentes: Rondelli, Daniel | Trigila, Mariano ::\n")
    fmt.Printf(":: Alumnos: Fariña, Gabriel | Godoy, Gonzalo | Saluta, Nahuel ::\n")
    fmt.Printf(" ... \n")
    fmt.Printf("Ingrese el numero que corresponde a las siguientes opciones y luego presione ENTER para gestionar: \n")
    fmt.Printf("1. Crear tablas.\n")
    fmt.Printf("2. Cargar PKS y FKS.\n")
    fmt.Printf("3. Cargar clientes, comercios y tarjetas predeterminados.\n")
    fmt.Printf("4. Cargar cierres.\n")
    fmt.Printf("5. Borrar FKS y PKS.\n")
    fmt.Printf("6. Cargar consumos de prueba.\n")

    fmt.Scanf("%s", &opcion)

    // verificamos la opcion elegida por el usuario.
    if opcion == "1" {
        createDatabase()
        crearTablas()
    } else if opcion == "2" {
        agregarPK()
        agregarFK()
    } else if opcion == "3" {
        cargarClientes()
        cargarComercios()
        cargarTarjetas()
    } else if opcion == "4" {
        cargarCierres()
    } else if opcion == "5" {
        borrarFKS()
        borrarPKS()
    } else if opcion == "6" {
        cargarConsumosVirtuales()
        autorizarConsumos()
    }
----

== Procedimiento para la generación de resúmenes (ejemplo)

[source, sql]
----
-- procedimiento para la generacion de los resumenes: recibe un numero de cliente y un periodo.
create or replace function generar_resumen(num_cliente int, periodo int) returns void as $$
declare
	nomb text;
	apell text;
	dir text;
	
	nombre_comercio text;
	terminacion_tarjeta int;
	periodo_desde date;
	periodo_hasta date;
	fecha_vto date;
	
	monto decimal;
	mont_final decimal;

	id_resumen int;
	id_linea int;
	nrotarj int;
	fecha_compra date;
	
	v record;
	j record;

begin
	-- buscamos el nombre, apellido y direccion del cliente, segun su numero de identificacion.
	select nombre into nomb from cliente where nrocliente = num_cliente;
	select apellido into apell from cliente where nrocliente = num_cliente;
	select domicilio into dir from cliente where nrocliente = num_cliente;

	-- FOR (cabecera) para recorrer cada una de las tarjetas de los clientes (en el caso de que tenga mas de una)
	id_resumen := 1;
	for v in select * from tarjeta where nrocliente = num_cliente loop
		-- obtenemos el ultimo digito de la tarjeta (terminacion).
		select right(v.nrotarjeta, 1) into terminacion_tarjeta;

		-- obtenemos el periodo de inicio, cierre y fecha de vencimiento.
		select fechainicio into periodo_desde from cierre where terminacion = terminacion_tarjeta::int and mes = periodo;
		select fechacierre into periodo_hasta from cierre where terminacion = terminacion_tarjeta::int and mes = periodo;
		select fechavto into fecha_vto from cierre where terminacion = terminacion_tarjeta::int and mes = periodo;

		-- FOR  (detalle) para obtener las compras y montos segun lo requerido
		-- generamos los consumos por periodo.
		id_linea := 1;
		mont_final = 0;
	    
	    
	   raise notice 'periodo desde: %', periodo_desde;
	   raise notice 'periodo_hasta: %', periodo_hasta;
	        
		for j in select * from compra where nrotarjeta = v.nrotarjeta and (fecha::date >= periodo_desde and fecha::date <= periodo_hasta) loop
	    
		    select nombre into nombre_comercio from comercio where nrocomercio = j.nrocomercio;
		    select j.fecha into fecha_compra;
		    select j.monto into monto;
			mont_final := mont_final + j.monto;

			-- insertamos la compra en el detalle.
			insert into detalle values(id_resumen, id_linea, fecha_compra, nombre_comercio, monto);
			id_linea := id_linea + 1;
		end loop;
		id_linea := 0;
		
		-- una vez hayamos agregado todos los detalles, insertamos la cabecera del resumen.
		insert into cabecera values (id_resumen, nomb, apell, dir, v.nrotarjeta, periodo_desde, periodo_hasta, fecha_vto, mont_final);
		id_resumen := id_resumen + 1;
	end loop;
end;
$$ language plpgsql;
----

== Trigger para la elaboración de alertas ante rechazos

[source, C]
----
-- trigger 1 de alertas: todo rechazo debe almacenarse automaticamente en la tabla de alertas
create function sp_alertaTrigger1() returns trigger as $$
begin
    insert into alerta values (default, new.nrotarjeta, now(), new.nrorechazo, 0, 'alerta: se rechazo la compra.');
return new;
end;
$$ language plpgsql;

-- trigger 1: creacion
create trigger tr_alerta1
before insert on rechazo
for each row
execute procedure sp_alertaTrigger1();
----

== Carga de cliente en JSON
[source, C]
----
cliente_1 := Cliente{1, "Juan", "Gomez", "9 de julio 1233", "1123554699"}
data1, err := json.Marshal(cliente_1)
if err != nil {
    log.Fatal(err)
}
----

== Conclusiones

El trabajo implicó un fuerte desafío, pues se debió investigar sobre el
uso del lenguaje _Go_, tipos de datos y procedimientos, además de
aprender los estándares de escritura del código.

Por otro lado, fortaleció la idea del trabajo en equipo y permitió la
comparación respecto del código generado con otros lenguajes (Python y
Java).
